from util import append_once, append_path_once, link, make_alias, DNF, PIP


def install_fonts() -> list[str]:
    return [
        'rm -rf ~/.fonts',
        link('assets/fonts', '~/.fonts'),
    ]


def install_shell() -> list[str]:
    return [
        *map(DNF, [
            'alacritty',
            'fish',
            'util-linux-user',
        ]),
        'mkdir -p ~/.config/fish',
        append_once('~/.config/fish/config.fish', 'fish_vi_keybindings'),
        append_once('~/.config/fish/config.fish', 'set fish_greeting'),
        'sudo chsh -s /usr/bin/fish master',
        'sudo chsh -s /usr/bin/fish',
        'sudo hostnamectl set-hostname slave',
    ]


def install_shell_tools() -> list[str]:
    autojump = [
        'rm -rf /tmp/autojump',
        'cd /tmp && git clone https://github.com/wting/autojump',
        'cd /tmp/autojump && ./install.py',
        append_once(
            '~/.config/fish/config.fish',
            'if test -f /home/master/.autojump/share/autojump/autojump.fish; . /home/master/.autojump/share/autojump/autojump.fish; end',
        ),
        'rm -rf /tmp/autojump',
    ]
    return [
        *[DNF('bpython'), make_alias('p', 'bpython')],
        *[
            DNF('ranger'),
            make_alias('f', 'ranger'),
            'mkdir -p ~/.config/ranger',
            'rm -f ~/.config/ranger/rc.conf',
            link('assets/ranger/rc.conf', '~/.config/ranger/rc.conf'),
        ],
        *[DNF('exa'), make_alias('k', 'exa -la')],
        *map(DNF, ['htop', 'perf']),
        *[
            DNF('neovim'),
            "sh -c 'curl -fLo \"${XDG_DATA_HOME:-$HOME/.local/share}\"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'",
            'rm -f ~/.config/nvim/init.vim',
            'rm -f ~/.config/nvim/coc-settings.json',
            'mkdir -p ~/.config/nvim',
            link('assets/nvim/init.vim',
                 '~/.config/nvim/init.vim',
                 is_symbolic=False),
            link('assets/nvim/coc-settings.json',
                 '~/.config/nvim/coc-settings.json',
                 is_symbolic=False),
            make_alias('e', 'nvim'),
            PIP('flake8'),
        ],
        PIP('httpie'),
        make_alias('c', 'clear'),
        *autojump,
    ]


def install_de() -> list[str]:
    return [
        'sudo dnf -y copr enable fuhrmann/i3-gaps &',
        'sudo dnf -y install i3-gaps rofi',
        'mkdir -p ~/.config/i3',
        'rm -f ~/.config/i3/config',
        link('assets/i3/config', '~/.config/i3/config', is_symbolic=False),
        'sudo dnf -y install compton xrandr',
        'rm -f ~/.config/compton.conf',
        'rm -f ~/.config/systemd/user/compton.service',
        'mkdir -p ~/.config/systemd/user',
        link('assets/compton/service',
             '~/.config/systemd/user/compton.service',
             is_symbolic=False),
        'systemctl --user enable compton',
        'systemctl --user start compton',
        'sudo dnf -y install dunst',
        'mkdir -p ~/.config/dunst',
        'rm -f ~/.config/dunst/dunstrc',
        link('assets/dunst/rc', '~/.config/dunst/dunstrc', is_symbolic=False),
        'sudo dnf -y install brightnessctl redshift feh',
        'sed -E \'s/\/redshift/\/redshift -l 59.93:30.31/\' assets/redshift/service > ~/.config/systemd/user/redshift.service',
        'systemctl --user enable redshift',
        'systemctl --user start redshift',
        'sudo dnf -y install chromium',
        'rm -rf ~/.config/wallpapers',
        link('assets/wallpapers', '~/.config/wallpapers'),
        'rm -f ~/.ideavimrc',
        link('assets/.ideavimrc', '~/.ideavimrc'),
    ]


def install_npm() -> list[str]:
    global_packages = '.npm-packages'
    no_sudo_vars = f'''
        set NPM_PACKAGES "$HOME/{global_packages}"
        set PATH $PATH $NPM_PACKAGES/bin
        set MANPATH $NPM_PACKAGES/share/man $MANPATH
    '''
    return [
        'sudo dnf -y install nodejs',
        f'mkdir -p "$HOME/{global_packages}"',
        f'npm config set prefix "$HOME/{global_packages}"',
        append_once('~/.config/fish/config.fish', no_sudo_vars),
    ]


def install_go() -> list[str]:
    url = 'https://golang.org/dl/go1.16.5.linux-amd64.tar.gz'
    basename = url.split('/')[-1]
    return [
        'sudo rm -rf /usr/local/go',
        f'cd /tmp && wget {url} && sudo tar -C /usr/local -xzf {basename}',
        append_path_once('/usr/local/go/bin')
    ]


def install_docker() -> list[str]:
    return [
        'sudo dnf -y install dnf-plugins-core',
        'sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo',
        'sudo dnf -y install docker-ce docker-ce-cli containerd.io',
        'sudo systemctl start docker',
        'sudo systemctl enable docker',
        'sudo usermod -aG docker $USER',
        'sudo wget https://github.com/bcicen/ctop/releases/download/0.7.6/ctop-0.7.6-linux-amd64 -O /usr/local/bin/ctop',
        'sudo chmod +x /usr/local/bin/ctop',
    ]
