import os.path
from os import getenv
from pathlib import Path

from invoke import run
from rich.progress import Progress


def DNF(pkg: str) -> str:
    return f'sudo dnf -y install {pkg}'


def PIP(pkg: str) -> str:
    return f'pip3 install {pkg}'


def NPM(pkg: str) -> str:
    return f'npm install -g {pkg}'


def run_many(*tasks: list[callable]):
    with Progress() as pb:
        for task in tasks:
            cmds = task()
            pbt = pb.add_task(task.__name__, total=len(cmds))
            for cmd in cmds:
                if type(cmd) is str and cmd:
                    pb.log(cmd)
                    run(cmd, hide=True)
                else:
                    cmd()
                pb.advance(pbt)


def append_once(path: str, text: str) -> callable:
    path = path.replace('~', str(Path.home()))

    def f():
        with open(path, 'a+') as file:
            contents = file.read()
            if text not in contents:
                file.write(contents)

    return f


def append_path_once(path: str) -> str:
    if path not in getenv('PATH'):
        return f'fish -c \'fish_add_path "{path}"\''
    return ''


def make_alias(alias: str, value: str) -> str:
    return f'fish -c \'alias {alias}="{value}" && funcsave {alias}\''


def link(src: str, dst: str, is_symbolic=True) -> str:
    src = Path(src).resolve()
    symbolic = '-s' if is_symbolic else ''
    return f'ln {symbolic} {src} {dst}'
