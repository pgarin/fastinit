call plug#begin()
Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdtree'
Plug 'Yggdroot/indentLine'
Plug 'ryanoasis/vim-devicons'
" Remove highlighting after search
Plug 'romainl/vim-cool'
" Visual marks display
Plug 'kshenoy/vim-signature'
Plug 'easymotion/vim-easymotion'
" f, F unique characters highlighting
Plug 'unblevable/quick-scope'
Plug 'matze/vim-move'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'joom/vim-commentary' 
Plug 'ervandew/supertab'
" Color scheme
Plug 'pgavlin/pulumi.vim'
Plug 'Vimjas/vim-python-pep8-indent'
call plug#end()

syntax on
filetype plugin on
filetype indent on

set noerrorbells
set novisualbell
set t_vb=
set tm=500

set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab

set number
set relativenumber

set nocompatible

set ruler
set colorcolumn=80

set background=dark
colorscheme pulumi

set showmatch " Braces

set mouse=a
set mousehide " When typing

set smartcase
set incsearch

set updatetime=100

let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'

let g:SuperTabDefaultCompletionType = "<c-n>"

set noswapfile
set nobackup

set autoread
au FocusGained,BufEnter * checktime

"nvim will create automatically - vim will not
set undodir=~/.config/nvim/undodir
set undofile

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,__pycache__,*.pyc,LICENSE,.idea

set viewoptions=folds,cursor
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent! loadview
autocmd BufRead *.* silent setlocal iminsert=0

let g:AutoPairsFlyMode = 1

let g:ale_linters = {
\   'python': ['flake8'],
\}

let g:ale_fixers = {
  \    'python': ['yapf'],
\}

let g:coc_global_extensions = [
    \ 'coc-python', 'coc-clangd'
\ ]

let g:ale_fix_on_save = 1

"Mappings
"Motion
""General
"j, k, h, l - std
nmap W gT
nmap E gt
imap <C-w> <Esc>W
imap <C-e> <Esc>e

""Overline
"0, $ - std
let g:qs_highlight_on_keys = ['f', 'F']
"`;` / ',' - repeat last search forward / backward

""Overwin
"H, M, L, %, g; (last edit jump) - std
"C-d, C-u, /, *, n, N - std
nmap t <Plug>(easymotion-overwin-f2)
nmap T <Plug>(easymotion-overwin-line)

""Overfile
"G, gg, <lineno>g - std
"m<markname>, '<markname>, m<space> (delete all) - std
nmap <S-j> 5j
nmap <S-k> 5k
nmap <S-s> ^
nmap <S-d> $

""Project scope
nmap <S-a> :NERDTreeToggle<CR>
imap <C-a> <Esc>:NERDTreeToggle<CR>

"Misc
let mapleader = " "
imap jj <Esc>
"Search fields etc.
cmap kk <C-^>
let g:AutoPairsShortcutBackInsert = 'kj'

nmap <leader>w :w!<cr>

let g:move_key_modifier = 'C'

hi Search cterm=NONE ctermfg=black ctermbg=yellow

"Close Vim if nothing but NERDTree left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
